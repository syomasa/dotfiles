#!/bin/bash

# Script to make paths to i3 configs, .vimrc configs, alacritty, and fonts easily manageable

vimrc_path=~/.vimrc
i3_path=~/.config/
i3blocks_path=~/.config/
alacritty_path=~/.config/
fonts_path=~/.fonts/

# use symbolic links
ln -s -r .vimrc $vimrc_path
ln -s -r ./gruvbox/.icons ~/.icons
ln -s -r ./gruvbox/.themes ~/.themes
ln -s -r ./gruvbox/alacritty/ $alacritty_path
ln -s -r ./gruvbox/i3blocks/ $i3blocks_path
ln -s -r ./gruvbox/i3/ $i3_path
ln -s -r ./fonts ~/.fonts
ln -s -r ./gruvbox/picom.conf ~/.config/picom.conf
ln -s -r ./.zshrc ~/.zshrc
