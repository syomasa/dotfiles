**Changes**

- Added `options hid_apple fnmode=2` into `/etc/modprobe.d/hid_apple.conf` this is to make f-keys work properly on varmilo's VA88m keyboard
