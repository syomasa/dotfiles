# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt notify
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/syomasa/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
PS1='%F{cyan}%B%2~%f%b %B%F{yellow}>%f%b '
alias ls='ls --color'
[ -f /opt/miniconda3/etc/profile.d/conda.sh ] && source /opt/miniconda3/etc/profile.d/conda.sh
