#!/bin/bash

shutdown='襤'
reboot='ﰇ'
logot=''

options="$shutdown\n$reboot\n$logot"
rofi_cmd="rofi -theme styles/power.rasi"

pick=$(echo -e "$options" | $rofi_cmd -dmenu -p "")

case $pick in
	$shutdown)
		systemctl poweroff
		;;
	$reboot)
		systemctl reboot
		;;
	$logot)
		i3-msg exit
		;;
esac
