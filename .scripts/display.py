"""
Script for easily setting up displays based on ammount of screens connected
"""
import subprocess
import re
from typing import List

def get_display_info() -> str:
    command = subprocess.Popen("xrandr", stdout=subprocess.PIPE)
    output = str(command.communicate()[0])

    return output

def get_name_and_status(output:str) -> List[str]:
    """
    parse display name and status from output
    """
    sub = re.sub("\\\\n", "\n", output) # make sure \n characters are really newline characters
    regex = re.findall("^.*connected", sub, re.M)
    displays = []

    for string in regex:
        name, status = string.split(" ")
        displays.append((name, status))

    return displays

#get_display_info()
def get_connected(output:str) -> List[str]:
    displays = get_name_and_status(output)
    connected = []

    for display in displays:
        if(display[1] == 'connected'):
            connected.append(display[0])

    return connected

if __name__ == "__main__":
    output = get_display_info()
    print(get_connected(output))
