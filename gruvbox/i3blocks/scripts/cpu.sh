#!/bin/bash

utilization_idle=$(sar -u 1 1| awk -F '[^0-9]*' 'NR==1{next}NR==2{next}NR==3{next}NR==4{print $14}')
total_percent=$(( 100-"$utilization_idle"))

echo CPU: $total_percent%
