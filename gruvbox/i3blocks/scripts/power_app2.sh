#!/bin/bash

charging=''
battery_100=''
battery_80=''
battery_50=''
battery_30=''
battery_10=''

BAT=$(acpi -b | grep -E -o '[0-9]*?%')
BAT_NUM=${BAT%?}
ICON=$battery_100
BAT_CHARGING=$(acpi -a | grep -Eo '([^: ]*)'| awk 'NR==3{print $1}')
COLOR="#8ec07c"

# Set icon
[ $BAT_NUM -le 100 ] && ICON=$battery_100
[ $BAT_NUM -le 80 ] && ICON=$battery_80
[ $BAT_NUM -le 50 ] && ICON=$battery_50 && COLOR="#fabd2f"
[ $BAT_NUM -le 30 ] && ICON=$battery_30 && COLOR="#fe8019"
[ $BAT_NUM -le 10 ] && ICON=$battery_10 && COLOR="#fb4934"

# Charging
if [ "$BAT_CHARGING" = "on-line" ]
then
	ICON=$charging
	COLOR="#8ec07c"
fi


# Full and short texts
echo "$ICON $BAT"
echo "$ICON $BAT"

[ $BAT_NUM -le 5 ] && exit 33
echo "$COLOR"

# Set urgent flag below 5% or use orange below 20%
exit 0
