#!/bin/bash

wlan_name=$(sed 's/"//g' <<< $(iwgetid | grep -Eo '"([^"]*)"' || echo "No connection"))

# full_text & short text
echo 直 $wlan_name
echo 直 $wlan_name

# set status color
[ "$wlan_name" != 'No connection' ] && echo "#458588"
[ "$wlan_name" == 'No connection' ] && echo "#fb4934"

