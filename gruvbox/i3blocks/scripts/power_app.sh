#!/bin/bash

battery_status=$(acpi -b | awk '{print $4}')
battery_percentile=$(sed 's/%,//' <<< ${battery_status})
ac_status=$(acpi -a | awk '{print $3}')

charging=''
battery_100=''
battery_80=''
battery_50=''
battery_30=''
battery_10=''

if [ "$ac_status" = "on-line" ]
then
	echo $charging $battery_percentile
elif [[ "$battery_percentile" -gt "80" ]]
then
	echo $battery_100 $battery_percentile'%'
elif [[ "$battery_percentile" -gt "50" ]]
then
	echo $battery_80 $battery_percentile'%'
elif [[ "$battery_percentile" -gt "30" ]]
then
	echo $battery_50 $battery_percentile'%'
elif [[ "$battery_percentile" -gt "10" ]]
then
	echo $battery_30 $battery_percentile'%'
else
	echo $battery_10 $battery_percentile'%'
fi
