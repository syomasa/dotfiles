" Auto setup for vim config makes it moveable across the systems
if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
				\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim')

Plug 'tpope/vim-dispatch'
Plug 'balanceiskey/vim-framer-syntax'
Plug 'franbach/miramare'
Plug 'sheerun/vim-polyglot'
" Plug 'tpope/vim-sensible'
" Plug 'Lokaltog/vim-powerline'
" Plug 'dracula/vim', { 'as': 'dracula' }
" Plug 'preservim/nerdtree' | Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'https://github.com/morhetz/gruvbox'
Plug 'ryanoasis/vim-devicons'
Plug 'vim-airline/vim-airline'
Plug 'tmsvg/pear-tree'
Plug 'ycm-core/YouCompleteMe'
Plug 'airblade/vim-gitgutter'
Plug 'Chiel92/vim-autoformat'

call plug#end()

" Make vim use gui colors
set termguicolors

"Encoding
set encoding=utf-8

" do some random shit makes powerline work :)
set t_Co=256

" Fancy icons for powerline
" let g:airline#extensions#tabline#enabled = 1

" disable italic for dracula
"let g:dracula_italic = 0
" Change colorscheme
let g:gruvbox_contrast_dark = 'hard'
set background=dark
colorscheme gruvbox

" Use 256 colours (Use this setting only if your terminal supports 256 colours)
set t_Co=256

" More natural splits
set splitbelow
set splitright

" Remap moving between splits
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" turn on autoindent
set autoindent

" set linenumbers
set number

" show matches while searching and higlight search
set incsearch
set hlsearch

" always show statusline
set laststatus=2

" Remove trailing whitespaces
autocmd BufWritePre * %s/\s\+$//e

" Automatically refresh nerd tree upon save
" autocmd BufWritePost * NERDTreeRefreshRoot

" set tab width
set shiftwidth=4
set tabstop=4

" auto open nerdtree plugin
" autocmd vimenter * NERDTree

" auto close nerdtree if only one left open
" autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" remap default behavior of o and O from newline + insert-mode --> newline
nnoremap o o<Esc>
nnoremap O O<Esc>

" setup dispatch on different filetypes
autocmd FileType c let b:dispatch = 'gcc -Wall -Werror -lm -o run %'
autocmd FileType cpp let b:dispatch = 'g++ -Wall -Werror -o run %'

" map CTRL+E to end of line, CTRL+a to start of line"
imap <C-e> <Esc>$1<right>
imap <C-a> <Esc>0i

"setup keybind save & compile command"
autocmd FileType c  nmap <F5> :wa<CR>:Dispatch<CR>
autocmd FileType cpp nmap <F5> :wa<CR>:Dispatch<CR>

" set minimum window height and width
set wmh=0
set wmw=0

" autoformat code upon save
"autocmd BufWrite * :Autoformat
"Format coden when F3 is pressed
noremap <F3> :Autoformat<CR>
